var Smoky = (function(){
    /*
     * used in init() to not run init() twice
     */
    var initalized = false;
    /*
     * separate separate parts of code into namespaces
     * save them here
     */
    var namespaces = {};
    /*
     * reused objects
     */
    var $window = $(window);
    var $document = $(document);
    var $sections = $('.menu-section');
    var $sectionsReversed = $($sections.get().reverse());
    var $menu = $('#menu');
    var $menuItems = $menu.children();
    var $videosection = $('#videosection');

    return {

        init: function() {
            /*
             * only run this.init() once
             */
            if ( initalized )
                return;

            initalized = true;
            /*
             * find and execute all namespaces
             */
            for ( var key in namespaces ) {

                if ( ! namespaces.hasOwnProperty(key) )
                    continue;

                namespaces[key]();
            }
        },
        /*
         * add namespace
         */
        namespace: function(key, namespace) {

            if ( ! key )
                throw new Error('Trying to set a namespace without an identifier.');
            if ( ! namespace )
                throw new Error('The actual namespace property missing for ' + obj.key + '.');
            if ( namespaces.hasOwnProperty(key) )
                throw new Error('Namespace with this identifier already exists.');

            namespaces[key] = namespace;
        },
        /*
         *
         */
        window: {
            object: $window,
            width: $window.width(),
            height: $window.height()
        },
        sections: {
            object: $sections,
            reversed: $sectionsReversed
        },
        document: {
            object: $document
        },
        menu: {
            object: $menu,
            items: $menuItems
        },
        videosection: {
            object: $videosection
        },
        /*
         * defined as screen smaller than 769
         */
        isMobile: function() {
            return this.window.width < 769
        }
    }
})();
