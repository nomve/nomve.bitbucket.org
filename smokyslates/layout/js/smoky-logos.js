Smoky.namespace( 'logos', function() {
    /*
     * general carousel options
     */
    var opts = {
        fx: 'carousel',
        autoHeight: 'calc', //calculate largest slide, use as height
    };
    /*
     * only run some slideshows when there is no space on the screen
     */
    var flexibleCarousels = {
        /*
         * list of all carousels
         */
        list: [],
        /*
         * add carousel to the list
         */
        add: function(obj) {
            this.init(obj);
            this.list.push(obj);
        },
        /*
         * initializing carousel
         */
        init: function(obj) {

            if ( ! (obj instanceof jQuery) )
                obj = $(obj);

            if ( this.needed(obj) )
                obj.cycle(opts)
            else
                obj.cycle('destroy');
        },
        /*
         * check if a carousel is needed
         */
        needed: function(obj) {

            var objWidth = obj.width();
            var contentWidth = ( function() {

                var children = obj.find('img');
                var width = 0;
                var checked = []; // list of checked elements
                children.each( function(index, element) {

                    element = $(element);
                    var src = element.attr('src');
                    /*
                     * only check new elements
                     * if the carousel already working, elements get multiplied
                     */
                    if ( $.inArray(src, checked) === -1 ) {
                        width += $(element).outerWidth(true);
                        checked.push(src);
                    }
                });

                return width;
            })(obj);
            /*
             * carousel needed if width of images larger than width of container
             */
            return contentWidth > objWidth;
        },
        /*
         * used on window resize to init/destroy carousels when needed
         */
        recheck: function() {
            for ( var i = 0, length = this.list.length; i < length; i++ )
                this.init(this.list[i]);
        }
    }
    /*
     * sometimes running
     */
    flexibleCarousels.add( $('#clients') );
    flexibleCarousels.add( $('#partners') );
    /*
     * recheck on resize
     */
    Smoky.window.object.on( 'resize', flexibleCarousels.recheck.bind(flexibleCarousels) );
});
