Smoky.namespace( 'popups', function() {

    /*
     * popup constructor
     */
    var Popup = function($obj) {
        /*
         * jQuery obj
         */
        this.$obj = $obj || {length: 0};
        this.height = function() {
            return this.$obj.outerHeight();
        }
        this.exists = function() {
            return this.$obj.length;
        };
        /*
         * things to do when added to the popups list
         */
        this.init = function() {

            this.setCallbacks();
            this.setAnimationOptions();
        }
        /*
         * open/closed
         */
        this.active = false;
        /*
         * openning/closing
         */
        this.toggle = function() {

            var action = ( this.active ) ? 'close' : 'open';
            this[action]();
        }
        this.open = function() {

            var self = this;
            /*
             * set the appropriate height on the whole container
             * and show content afterwards
             */
            this.resize({
                height: this.height(),
                speed: this.animationOptions.speed1,
                easing: this.animationOptions.easing1,
                callback: function() {
                    /*
                     * animate our popup with its animation
                     */
                    self.$obj.animate({
                        left: 0
                    }, self.animationOptions.speed2, self.animationOptions.easing2);
                    /*
                     * at the same time fade out the main content
                     */
                    self.$main.animate({
                        opacity: 0
                    }, self.animationOptions.speed2 );
                }
            });
            this.active = true;
        };
        this.close = function() {

            var self = this;
            /*
             * animate the popup out of view
             */
            var left = this.normalPopupPosition;
            this.$obj.animate({
                left: left
            }, this.animationOptions.speed1, this.animationOptions.easing1, function() {
                /*
                 * set parent to normal height
                 * fadein main content
                 */
                self.resize({
                    height: self.normalParentHeight(),
                    speed: self.animationOptions.speed2,
                    easing: self.animationOptions.easing2,
                    callback: function() {
                        /*
                         * remove fixed height and left position
                         */
                        self.$parent.css({height: 'auto'});
                        self.$obj.css({left: ''});
                    }
                });
                /*
                 * fadein content
                 */
                self.$main.animate({
                    opacity: 1
                }, self.animationOptions.speed2 );
            });
            this.active = false;
        };
        /*
         * callbacks to set
         */
        this.setCallbacks = function() {

            $obj.find('.popup-back').on( 'click', this.close.bind(this) );
        };
        /*
         * animation options
         */
        this.animationOptions = {};
        this.setAnimationOptions = function() {

            var options = this.$parent.data();

            this.animationOptions.speed1 = options.smokyPopupSpeed1 || 300;
            this.animationOptions.speed2 = options.smokyPopupSpeed2 || 500;
            this.animationOptions.easing1 = options.smokyPopupEasing1 || 'swing';
            this.animationOptions.easing2 = options.smokyPopupEasing2 || 'swing';
        };
        /*
         * reference the parent
         * and the main content container
         */
        this.$parent = $obj.parent();
        this.$main = $obj.siblings('.services-main');
        /*
         * remember the normal parent height
         * used when closing
         */
        this.normalParentHeight = function() {

            var currentHeight = this.$parent.height();
            var normalHeight = this.$parent.css({height: 'auto'}).height();

            this.$parent.css({height: currentHeight});

            return normalHeight;
        }
        this.normalPopupPosition = this.$obj.css('left');
        /*
         * scroll to element when finished opening/closing
         */
        this.scrollTo = function() {
            var element = this.active ? this.$obj : this.$parent;

            $('html, body').animate({
                scrollTop: element.offset().top - $('#header').outerHeight()
            }, 400);
        };
        this.scrollToTimeout = null;
        /*
         * set the right height when showing
         */
        this.resize = function( options ) {

            if ( ! options.height )
                throw new Error('No new element height defined.');

            var defaults = {
                speed: 0,
                easing: 'swing',
                scrollTo: true
            };
            var settings = $.extend( {}, defaults, options );
            var callback = settings.callback || new Function();
            /*
             * resize parent to new height
             */
            this.$parent
                .stop( true, false )
                .animate({
                    height: settings.height
                }, settings.speed, settings.easing, callback.bind(this) );
            /*
             * scroll to top when finished
             */
            if ( ! settings.scrollTo )
                return;

            var delay = settings.speed;
            if ( this.scrollToTimeout ) {
                clearTimeout( this.scrollToTimeout );
                this.scrollToTimeout = null;
            }
            this.scrollToTimeout = setTimeout( this.scrollTo.bind(this), delay );
        };
    }

    /*
     * specific popups
     */
    var popups = {
        /*
         * all of the popups
         */
        list: {},
        /*
         * add a popup to the list
         */
        add: function(key,obj) {

            if ( this.list.hasOwnProperty(key) || ! obj.exists )
                return;

            obj.init();
            this.list[key] = obj;
        },
        show: function(key) {

            if ( ! this.list.hasOwnProperty(key) )
                return;

            this.list[key].toggle();
        },
        /*
         * currently open popup
         */
        current: function() {

            for ( var key in this.list ) {

                if ( ! this.list.hasOwnProperty(key) )
                    continue;

                var popup = this.list[key];
                if ( popup.exists && popup.active )
                    return popup;
            }

            return null;
        },
        /*
         * what happens on window resize
         */
        onResize: function( scrollTo ) {

            var popup = this.current();
            if ( ! popup )
                return;

            var options = {
                height: popup.height()
            }
            if ( scrollTo !== undefined  )
                options.scrollTo = scrollTo;

            popup.resize( options );
        }
    };

    /*
     * resize popup container on window resize
     * popups are 'absolute' positioned
     * do it every 100ms
     */
    var check = false;
    var scrollingStop = null;
    var interval = setInterval( function() {
        check = true;
    }, 100 );
    Smoky.window.object.on( 'resize', function() {

        if ( check ) {
            check = false;
            popups.onResize();
        }
    });
    /*
     * don't allow window.onresize to fire when scrolling
     * happens on mobile because the address bar goes out of focus while scrolling
     */
    Smoky.window.object.on( 'scroll', function() {

        check = false;
        clearInterval( interval );

        if ( scrollingStop ) {
            clearTimeout( scrollingStop );
            scrollingStop = null;
        }
        /*
         * wait 200ms to allow resizing again
         */
        scrollingStop = setTimeout( function() {

            check = true;
            interval = setInterval( function() {
                check = true;
            }, 100 );
            popups.onResize(false)
        }, 400 );
    });

    /*
     * links to use to init popups
     */
    var links = $('.services-link');
    /*
     * helper function to extract the id from the href
     */
    var getId = function(el) {

        if ( ! el )
            return;
        if ( !(el instanceof jQuery) )
            el = $(el);

        var href = el.attr('href');
        if ( ! href )
            return false;

        var id_a = href.split('#');
        var id = id_a[id_a.length - 1];

        return id;
    }
    /*
     * find and init popups
     */
    links.each( function(index, el) {

        var id = getId( el );
        if ( ! id )
            //jquery each continue
            return true;

        var popupElement = document.getElementById( id );
        if ( ! popupElement || !(popupElement instanceof HTMLElement) )
            return true;
        /*
         * init popup and place in list
         */
        var popupElement = $(popupElement);
        var popup = new Popup( popupElement );
        popups.add( id, popup );
    });

    /*
     * show
     */
    links.on( 'click', function(e) {

        e.preventDefault();

        var id = getId( $(this) );
        if ( ! id ) {
            console.log( 'no popup linked' );
            return;
        }

        popups.show(id);
    });

});
