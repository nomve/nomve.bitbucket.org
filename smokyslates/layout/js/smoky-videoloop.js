Smoky.namespace( 'mainvideo', function() {
    /*
     * adjusts the margin of the video
     * if the height doesn't fit in the container
     */
    var resizer = function(e) {
        /*
         * section height is adaptable
         * sometimes the video will be cut off
         */
        var sectionHeight = section.height();

        var video = section.children('video');
        var videoHeight = video.height();

        var negativeMargin = (sectionHeight-videoHeight) / 2;
        /*
         * set margin
         */
        video.css({
            marginTop: negativeMargin
        });
    }
    /*
     * what and where to insert
     */
    var section = Smoky.videosection.object;
    if ( ! section.length )
        return;
    var videoData = section.data();
    if ( ! videoData )
        return;

    var videoUrl = videoData.smokyVideoUrl;
    /*
     * append video
     */
    if ( Modernizr.video
            && videoUrl ) {

        var videoSettings = videoData.smokyVideoSettings;
        var videoClass = videoData.smokyVideoCssClass;
        var animationSpeed = videoData.smokyVideoAnimationSpeed || 750;
        var animationEasing = videoData.smokyVideoAnimationEasing || 'swing';
        /*
         * create video element;
         */
        $.ajax({
            url: videoUrl
        })
        .done( function() {

            var $video = $('<video class="' + videoClass + '" ' + videoSettings + ' />');
            var video = $video[0];
            video.src = videoUrl;
            /*
             * wait until the video starts playing to set margins and show
             */
            $video.one( 'playing', function() {
                /*
                 * set margin
                 */
                resizer();
                /*
                 * animate
                 */
                var sectionHeight = section.height();
                section
                    .css({
                        height: 0
                    })
                    .animate({
                        height: sectionHeight
                    }, animationSpeed, animationEasing, function() {
                        /*
                         * remove fixed height when finished
                         */
                        section.css({height: 'auto'});
                        Smoky.document.object.trigger( 'videoReady' );
                    });
            });
            /*
             * append to container
             * will play automatically if set in html
             */
            section.append($video);
        })
        .fail( function() {
            Smoky.document.object.trigger( 'videoReady' );
        });;
    }
    /*
     * adjust margin on window resize
     */
    Smoky.window.object.on( 'resize', resizer );
});
