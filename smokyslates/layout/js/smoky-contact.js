Smoky.namespace( 'contact', function() {

    /*
     * use svg for the map if available
     */
    if ( Modernizr.svg ) {

        var map = $('#map');
        var mapData = map.data();

        if ( mapData ) {

            var svgImg = mapData.smokyMapSvg;

            if ( svgImg ) {
                /*
                 * only do it if you can actually load the file
                 */
                $.get( svgImg, function() {
                    map.attr('src', svgImg);
                });
            }
        }
    }
});
