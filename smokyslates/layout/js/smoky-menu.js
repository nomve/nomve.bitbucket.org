Smoky.namespace( 'menu', function() {

    var link = $('#menulink');
    var linkData = link.data();
    var linkSpeed = linkData.smokyMenuButtonSpeed;
    var linkEasing = linkData.smokyMenuButtonEasing;

    var menu = Smoky.menu.object;
    var menuData = menu.data();
    var menuSpeed = menuData.smokyMenuSpeed || 700;
    var menuEasing = menuData.smokyMenuEasing || 'swing';
    var menuFixedActive = menuData.smokyMenuFixedActive;

    var menuCommands = {
        /*
         * keep track if the menu is open or closed
         */
        isOpen: false,
        /*
         * no multiple clicks between animations
         */
        isLocked: false,
        /*
         * opening the menu
         */
        open: function() {
            var self = this;
            var menuWidth = menu.outerWidth();
            /*
             * animate button to right
             * open the menu when finished
             */
            link.animate({
                left: menuWidth
            }, linkSpeed, linkEasing, function() {
                menu.slideDown({
                    duration: menuSpeed,
                    easing: menuEasing,
                    complete: function() {
                        //all animations done
                        self.isLocked = false;
                    }
                });
            });

            this.isOpen = true;
        },
        /*
         * close the menu
         */
        close: function() {

            var self = this;

            menu.slideUp({
                duration: menuSpeed,
                easing: menuEasing,
                complete: function() {
                    /*
                     * slideDown sets display block
                     * slideUp display none
                     * remove display none, let css class take care of displaying or hiding
                     */
                    menu.css({display: ''});
                    /*
                     * move button back
                     */
                    link.animate({
                        left: 0
                    }, linkSpeed, linkEasing, function() {
                        //all animations done
                        self.isLocked = false;
                    });
                }
            });
            this.isOpen = false;
        }
    }

    /*
     * show/hide menu on click
     */
    link.on( 'click', function(e) {

        e.preventDefault();

        if ( menuCommands.isLocked )
            return;

        menuCommands.isLocked = true;
        /*
         * select the right function
         */
        (menuCommands.isOpen) ? menuCommands.close() : menuCommands.open();

    });

    /*
     * setting and clearing active menu position
     */
    var activeMenu = {
        /*
         * indicator for the active element
         */
        element: null,
        /*
         * index of the current active element
         */
        current: null,
        /*
         * append indicator to the menu
         */
        append: function() {

            if ( ! this.element )
                this.element = $('<span class="menu-active" />')
                                .appendTo(Smoky.menu.object);

            return this.element.fadeIn( this.speed );

        },
        /*
         * set current active element
         */
        active: function(index, done) {

            this.current = index;
            Smoky.menu.items
                .removeClass('current-section')
                .eq(index).addClass('current-section');

            this.move(index, done);
        },
        /*
         * movement speed
         */
        speed: Smoky.menu.object.data().smokyMenuSpeed,
        /*
         * move indicator to the active item
         */
        move: function(index, doneCallback) {
            /*
             * move can be called with or without explicit index
             * for instance on hover is with
             */
            if ( typeof index === 'undefined' )
                index = this.current;
            /*
             * if still no index, we are on top of the page
             * nothing is active
             */
            if ( typeof index !== 'number' ) {
                this.clear();
                return;
            }
            /*
             * calculate position to move
             */
            var menuItem = Smoky.menu.items.eq(index);
            var position = menuItem.position();
            var padding = parseInt( menuItem.css('padding-left'), 10 );
            /*
             * width of indicator is width of menu element
             */
            var elementWidth = menuItem.width();
            /*
             * make sure the element is appended
             */
            this.append()
                    .stop(true, false)
                    .animate({
                        left: position.left + padding,
                        opacity: 1,
                        width: elementWidth
                    }, this.speed);

            /*
             * call when finished animation
             * can't do it as a realy animate callback
             * stop( true, false ) can cancel the animation
             * and the callback will not be called
             */
            if ( doneCallback && typeof doneCallback === 'function' ) {
                setTimeout( doneCallback, this.speed );
            }
        },
        /*
         * nothing active
         */
        clear: function() {
            if ( ! this.element )
                return;

            this.element.animate({
                opacity: 0,
                left: 0,
                width: 0
            }, this.speed);
            this.current = null;
        },
        /*
         * disable check on scroll
         */
        autoCheck: true,
        /*
         * actually checking for the current active element
         */
        check: function() {

            if ( ! this.autoCheck )
                return;

            var self = this;
            /*
             * make sure an active element is appended
             */
            this.append();
            /*
             * section counts as active when its top border is 25% away from window top
             */
            var start = parseInt( Smoky.window.height * .33 );
            /*
             * loop through sections to find the best active match
             * best means top position of element smaller than % of window height
             * back to front so no need to check if position is positive
             */
            var found = false;
            Smoky.sections.reversed.each( function(index,element) {

                var offset = $(element).offset();
                var position = offset.top - Smoky.document.object.scrollTop();

                if ( position < start ) {
                    /*
                     * index is in reversed order of the actual elements
                     */
                    var trueIndex = Smoky.sections.object.length - 1 - index ;
                    self.active(trueIndex);
                    found = true;
                    return false;
                }
            });
            /*
             * nothing found
             */
            if ( ! found )
                this.clear();
        }
    };
    /*
     * called when page ready
     */
    var settingMenuActive = function() {
        /*
         * how and when to scroll to a section of the page
         */
        var scrollToSection = function(e) {
            /*
             * find to section from href
             */
            var $this = $(this);
            var $link = $(this).children('a');
            var href = $link.attr('href');
            /*
             * take just the identifier, always without /
             */
            href = href.replace('/','').split('#');
            var id = href[ href.length-1 ];
            var currentPath = window.location.pathname.replace('/','');
            /*
             * allow going to another page
             */
            if ( currentPath !== href[0] )
                return;
            /*
             * otherwise we stay on the page, scroll to another section
             */
            e.preventDefault();
            /*
             * close the menu on mobile
             */
            if ( Smoky.isMobile() )
                menuCommands.close();

            var element = $( document.getElementById(id) );
            if ( ! element.length )
                return;

            /*
             * some elements can never become active in the menu when scrolled to
             * always set manually on click
             */
            activeMenu.autoCheck = false;
            var onDone = function() {
                activeMenu.autoCheck = true;
            }
            activeMenu.active( $this.index(), onDone );
            /*
             * calculate position and scroll
             */
            var elementOffset = element.offset();
            var length = elementOffset.top - $('#header').outerHeight();
            $('html,body').animate({
                scrollTop: length
            }, 500);
        }

        /*
         * clicking on menu links
         */
        Smoky.menu.items.on( 'click', scrollToSection );

        if ( menuFixedActive )
            activeMenu.active( menuFixedActive );
        /*
         * one time check for active item
         */
        else {
            /*
             * try to find active section
             */
            var hash = window.location.hash;
            if ( hash ) {
                var menuItem = Smoky.menu.items.children('[href*="' + hash + '"]').parent();
                if ( menuItem.length )
                    menuItem.click();
            }
            else
                activeMenu.check();
            /*
             * sets active menu items based on window position
             * only do it every x seconds
             */
            var check = false;
            var interval = setInterval( function() {
                check = true;
            }, 100 );
            Smoky.window.object.on( 'scroll', function() {
                if ( check ) {
                    check = false;
                    activeMenu.check();
                }
            });
        }
        /*
         * move active menu indicator on hover
         */
        Smoky.menu.items.hover(
            function () {
                activeMenu.move( $(this).index() );
            },
            function () {
                activeMenu.move();
            }
        );
    }
    /*
     * wait for video to finish loading/animating if there
     * so we always get accurate position
     */
    if ( Smoky.videosection.object.length )
        Smoky.document.object.on( 'videoReady', settingMenuActive );
    else
        settingMenuActive();
});
