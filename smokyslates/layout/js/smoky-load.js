//load scripts
Modernizr.load([
    {
        /*
         * load jquery first
         */
        load: [
            'layout/js/jquery.min.js'
        ],
        complete: function() {
            /*
             * load everything else
             */
            Modernizr.load([
                {
                    load: [
                        'layout/js/jquery.cycle2.min.js',
                        'layout/js/jquery.cycle2.carousel.min.js',
                        'layout/js/jquery.easing.1.3.js',
                        'layout/js/smoky.js',
                        'layout/js/smoky-logos.js',
                        'layout/js/smoky-videoloop.js',
                        'layout/js/smoky-menu.js',
                        'layout/js/smoky-contact.js',
                        'layout/js/smoky-popups.js',
                    ],
                    complete: function() {

                        $(document).ready( Smoky.init );
                    }
                }
            ]);
        }
    }
]);
