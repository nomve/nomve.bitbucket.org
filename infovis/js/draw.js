var Draw = ( function() {
    /*
     * jQuery object containing the graph legend
     */
    var _legend = null;
    /*
     * jQuery object, canvas to draw on
     */
    var _canvas = null;
    /*
     * icon/column width
     */
    var _blockWidth = 15;
    var _blockHeight = 15;
    /*
     * jQuery object, storing points on canvas
     */
    var _layers = {
        points: null,
        rebounds: null,
        assists: null
    };
    var getLayer = function(key) {

        if ( ! key )
            throw new Error('Trying to get a layer to draw on, without specifying which one.')

        if ( _layers[key] && _layers[key] instanceof jQuery )
            return _layers[key];

        if ( ! _canvas )
            throw new Error('Canvas element to draw on was not set. Draw.canvas = HTMLElement || jQuery');

        _layers[key] = $('<div class="' + key + '-layer canvas-layer" />')
                            .css({
                                display: 'none'
                            })
                            .data('key', key);

        return _layers[key];
    };
    /*
     * insert minutes on the layer
     */
    var insertMinute = function(options) {

        if ( ! (options
               && typeof options.index == 'number'
               && options.layer) )
            throw new Error('Layer to draw on and index needed to insert one minute');

        var layer = options.layer;
        var index = options.index;
        var count = options.count;
        var time = index+1;
        var left = _blockWidth * index;

        var minute = $('<div class="minute"><div class="time">' + time  + '</div>')
                        .css({
                            left: left,
                            display: 'none'
                        })
                        .appendTo(layer);
        if ( count )
            minute.data('count', count);

        return minute;
    }
    /*
     * draws a layer
     */
    var drawLayer = function(stat) {
        var key = stat.type;
        var layer = getLayer(key);
        /*
         * complete game data
         */
        var gameData = Game.data;
        /*
         * display the layer when all layer points inserted
         */
        layer.one( _events.POINTS_INSERTED, function() {
            layer.slideDown( function() {
                var cols = $(this).find('.minute');
                cols.each( function(index, item) {
                    setTimeout( function() {
                        $(item).fadeIn();
                    }, index*10);
                });
                /*
                 * signal that the points have been drawn
                 */
                setTimeout( function() {
                    _canvas.trigger( _events.POINTS_DRAWN );
                }, cols.length * 20);
            });
        });
        /*
         * highest point differential
         */
        var maxDiff = 0;
        /*
         * go through quarters
         */
        var counter = 0;
        for ( var i = 0, dlength = gameData.length; i < dlength; i++ ) {

            var quarter = gameData[i];
            var period = quarter.period;
            var minutes = quarter.minutes;
            /*
             * go through minutes
             */
            for ( var j = 0, mlength = minutes.length; j < mlength; j++ ) {

                var minute = minutes[j];
                var currentMinute = minute.minute;
                /*
                 * insert a minute container
                 */
                var column = insertMinute({
                    layer: layer,
                    index: counter++,
                    count: minute[key].count
                });
                /*
                 * get differential
                 * to know how many points to insert
                 */
                var teams = Game.teams;
                var diff = ( function(count) {
                    return count[teams.home.id] - count[teams.away.id];
                })( minute[key].count );
                /*
                 * set colors based on differential
                 */
                var color1 = null, color2 = null, cssClass = null;
                /*
                 * home team in the lead
                 */
                if ( diff > 0 ) {
                    color1 = teams.home.color;
                    color2 = teams.away.color;
                    cssClass = 'color-home';
                }
                else {
                    color1 = teams.away.color;
                    color2 = teams.home.color;
                    cssClass = 'color-away';
                }
                /*
                 * no negative diff values
                 */
                diff = Math.abs(diff);
                column.data('pointDiff', diff);
                if ( diff > maxDiff )
                    maxDiff = diff;
                /*
                 * and in the end add the points
                 */
                for ( var k = 0; k < diff; k++ ) {
                    var bottom = _blockHeight * k +1;
                    $('<div class="point" />')
                        .addClass( cssClass )
                        .css({
                            bottom: bottom,
                            background: color1
                        })
                        .appendTo(column);
                }
            }
        }
        /*
         * set height on the layer
         * based on the highest vertical point
         */
        layer.css({
            height: maxDiff * _blockHeight
        });
        /*
         * insert horizontal lines to help identify points
         */
        var gridDistance = stat.grid;
        var start = maxDiff-1 - ( (maxDiff-1) % gridDistance );
        for ( var i = start; i >= 0; i = i-gridDistance ) {
            $('<div class="graph-grid point-line"><div class="number">' + i + '</div></div>')
                .css({
                    bottom: i * _blockHeight -1
                })
                .prependTo( layer );
        };
        /*
         * trigger inserted events
         */
        layer.trigger( _events.POINTS_INSERTED );
        /*
         * return the layer
         * so it can be appended
         */
        return layer;
    }
    /*
     * list of event constants
     */
    var _events = {
        POINTS_INSERTED: 'pointsInserted',
        POINTS_DRAWN: 'pointsDrawn'
    }
    /*
     * all the event handlers for the canvas
     */
    var attachListeners = function() {

        /*
         * for the click event
         * remember the first and second one clicked
         */
        var selected = {
            points: [],
            rebounds: [],
            assists: []
        };
        var scoreBox = {
            points: null,
            rebounds: null,
            assists: null
        };;

        $(document).on( 'mouseenter', '.minute',
            /*
             * hovering minutes
             */
            function() {
                    var minute = $(this);
                    /*
                     * show current score on hover
                     */
                    var count = minute.children('.count');
                    if ( ! count.length ) {
                        var data = minute.data();
                        if ( ! data || ! data.count ) {
                            return;
                        }
                        /*
                         * append if not already there
                         */
                        var homeColor = Game.home.color;
                        var awayColor = Game.away.color;
                        var homeScore = data.count[Game.home.id];
                        var awayScore = data.count[Game.away.id];
                        count = $('<div class="count">' +
                                        '<div class="team-count home-count color-home" style="background: ' + homeColor + '">' + homeScore + '</div>' +
                                        '<div class="team-count away-count color-away" style="background: ' + awayColor + '">' + awayScore + '</div>' +
                                    '</div>')
                                    .css({
                                        display: 'none'
                                    })
                                    .appendTo(minute);
                        /*
                         * set position
                         */
                        if ( data.hasOwnProperty('pointDiff') ) {
                            //count.css({
                            //    bottom: data.pointDiff * _blockHeight + 30
                            //})
                        }

                    }
                    count.fadeIn();
                    minute.addClass('active');
                }
            )
            .on( 'mouseleave', '.minute',
                function() {
                    var minute = $(this);
                    minute
                        .removeClass('active')
                        .children('.count')
                            .stop()
                            .hide();
                }
            )
            /*
             * show point differential between two minutes
             */
            .on( 'click', '.minute', function(e) {

                var minute = $(this);
                var key = minute.parent().data().key;
                var current = selected[key];
                /*
                 * reset everything if already clicked on two minutes previously
                 */
                if ( current.length == 2 ) {
                    for ( var i = 0; i < current.length; i++ ) {
                        current[i].removeClass('selected');
                    }
                    selected[key] = [];
                    scoreBox[key].hide();
                    return;
                }
                /*
                 * reset on second click
                 */
                if ( minute.hasClass('selected') ) {
                    minute.removeClass('selected');
                    current = [];
                    return;
                }
                /*
                 * make it selected
                 */
                minute.addClass('selected');
                current.push(minute);
                /*
                 * connect if clicked on two
                 */
                if ( current.length == 2 ) {

                    //partial score
                    var score = {};
                    var homeId = Game.home.id;
                    var awayId = Game.away.id;

                    var count = current[0].data().count;
                    score[homeId] = count[homeId];
                    score[awayId] = count[awayId];

                    count = current[1].data().count;
                    score[homeId] = Math.abs( score[homeId] - count[homeId] );
                    score[awayId] = Math.abs( score[awayId] - count[awayId] );

                    var homeColor = Game.home.color;
                    var awayColor = Game.away.color;
                    /*
                     * insert box or update score
                     */
                    if ( ! scoreBox[key] || scoreBox[key].length ) {
                        scoreBox[key] = $( '<div class="scorebox">' +
                                            '<div class="count">' +
                                                '<div class="team-count home-count color-home" style="background: ' + homeColor + '">' + score[homeId] + '</div>' +
                                                '<div class="team-count away-count color-away" style="background: ' + awayColor + '">' + score[awayId] + '</div>' +
                                            '</div>' +
                                        '</div>' )
                                        .css({
                                            display: 'none'
                                        })
                                        .appendTo(_layers[key]);
                    }
                    else {
                        scoreBox[key]
                            .find('.home-count').text( score[homeId] )
                            .next('.away-count').text( score[awayId] );
                    }
                    /*
                     * update scorebox position
                     */
                    var left1 = current[0].position().left;
                    var left2 = current[1].position().left;
                    var width = Math.abs( left1 - left2 ) - _blockWidth;
                    var left = ( left1 > left2 ) ? left2 : left1;
                    left += _blockWidth;
                    scoreBox[key].css({
                        left: left,
                        width: width
                    }).fadeIn();
                }

            });
    }


    return {
        /*
         * list of event constants
         */
        get events() {
            return _events;
        },
        /*
         * reset all settings/data
         */
        reset: function() {
            /*
             * remove the elements
             */
            if ( _legend instanceof jQuery )
                _legend.remove();
            _legend = null;
            /*
             * reselect the currently visible layers
             * and delete them
             */
            $('.canvas-layer').slideUp( UI.settings.changeSpeed, function() {
                if ( _canvas instanceof jQuery )
                    _canvas.empty();
                for ( var key in _layers )
                    _layers[key] = null
            });
            /*
             *
             */
            $(document).off();
        },
        /*
         * draw team names and color code
         */
        legend: function() {
            /*
             * don't do anything without canvas
             */
            if ( ! _canvas )
                throw new Error('Canvas to draw on not set');
            /*
             * check if it was somehow already drawn
             */
            if ( _legend && _legend instanceof jQuery ) {
                _legend.fadeIn();
                return;
            }

            _legend = $('<div class="legend" />')
                            .css({
                                display: 'none'
                            });

            var teams = Game.teams;

            for ( var key in teams ) {
                var team = teams[key];
                _legend.append( team.teamLabel(key) );
            }

            _canvas.append(_legend);
            /*
             * show legend once all points inserted
             */
            _canvas.one( this.events.POINTS_INSERTED, function() {
                _legend.fadeIn();
            });
        },
        /*
         * get/set canvas
         */
        get canvas() {
            return _canvas;
        },
        set canvas(value) {
            /*
             * convert to jQuery
             */
            if ( value instanceof HTMLElement )
                value = $(value);

            if ( value instanceof jQuery )
                _canvas = value;
            else
                throw new TypeError('Invalid element type for canvas. Either HTMLElement or jQuery needed');
        },
        /*
         * graphing the points
         */
        graphPoints: function() {
            /*
             * which layers to show
             */
            var statTypes = UI.statTypes;
            /*
             * display all the wanted layers
             */
            for ( var key in statTypes ) {
                var stat = statTypes[key];
                if ( ! stat.visible )
                    continue;
                var layer = drawLayer(stat);
                _canvas.append(layer);
            }
            /*
             * attach listeners for hovering etc.
             */
            attachListeners();
            _canvas.trigger( _events.POINTS_INSERTED );
        },
        showLayer: function(key) {
            /*
             * draw the layers if not present
             */
            if ( ! _layers[key] ) {
                var layer = drawLayer( UI.statTypes[key] );
                /*
                 * right (richtige, nicht rechte) layer position
                 * my brain is not working anymore
                 */
                if ( key === 'rebounds' && _layers.points ) {
                    _layers.points.after( layer );
                    return;
                }
                else if ( key === 'assists' && _layers.rebounds ) {
                    _layers.rebounds.after( layer );
                    return;
                }
                else if ( key === 'assists' && _layers.points ) {
                    _layers.points.after( layer );
                    return;
                }
                _canvas.prepend( layer );
            }
            else {
                _layers[key].slideDown();
            }
        },
        hideLayer: function(key) {

            if ( _layers[key] ) {
                _layers[key].slideUp();
            }
        },
        showPlayer: function(playerId) {

            _canvas.on( _events.POINTS_INSERTED, function() {

                Draw.hidePlayer();
                var gameData = Game.data;
                var statTypes = UI.statTypes;
                var playerId = parseInt( UI.player.val(), 10 );

                for ( var key in statTypes ) {
                    var stat = statTypes[key];
                    if ( ! stat.visible )
                        continue;

                    var $minute = $('.' + key + '-layer .minute');

                    for ( var i = 0, dlength = gameData.length; i < dlength; i++ ) {

                        var quarter = gameData[i];
                        var minutes = quarter.minutes;

                        for ( var j = 0, mlength = minutes.length; j < mlength; j++ ) {

                            var plays = minutes[j][key].plays;
                            var minute = minutes[j].minute;
                            var toAdd = 0;
                            for ( var k = 0, plength = plays.length; k < plength; k++ ) {
                                var play = plays[k];
                                if ( play.playerId === playerId ) {
                                    toAdd += play.points ? play.points : 1;
                                    if ( playerId === 201168 ) {
                                        //console.log(toAdd, minute);
                                    }
                                }
                            }
                            /*
                             * insert into minute
                             */
                            if ( toAdd ) {
                                $minute.eq(i*12+j).append(
                                    ( function(toAdd,height) {
                                        var elements = '';
                                        for ( var p = 0; p < toAdd; p++ ) {
                                            var bottom = p*height + 1;
                                            elements += '<div class="point player-point" style="bottom: ' + bottom + 'px" />';
                                        }
                                        return elements;
                                    })(toAdd,_blockHeight)
                                );
                            }
                        }

                    }
                }
            });
        },
        hidePlayer: function() {
            $('.player-point').remove();
        }
    }
})();
