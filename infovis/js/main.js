( function($) {

    $(document).ready( function() {

        Draw.canvas = $('#canvas');
        UI.loader = $('#loader');
        UI.statTypes = $('input[name="stats[]"]');
        UI.player = $('#player');
        UI.showGraph();
    });

})(jQuery)
