var Cache = ( function() {
    /*
     * team name cache
     */
    var _teamNames = {
        1610612759: {
            name: 'San Antonio Spurs'
        },
        1610612748: {
            name: 'Miami Heat'
        }
    }
    /*
     * color cache
     */
    var _teamColors = {}
    /*
     *
     */
    return {
        get teamNames() {
            return _teamNames;
        },
        set teamNames(value) {

            if ( _teamNames instanceof Object ) {
                for ( var key in value ) {
                    _teamNames[key] = value[key];
                }
            }
        },
        get teamColors() {
            return _teamColors;
        },
        set teamColors(value) {

            if ( _teamColors instanceof Object ) {
                for ( var key in value ) {
                    _teamColors[key] = value[key];
                }

            }
        },
        /*
         * check for color of a specific team
         */
        teamColor: function(teamId) {

            if ( _teamColors[teamId] ) {
                return _teamColors[teamId];
            }
        }
    }
})();
