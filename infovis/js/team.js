function Team(options) {
    /*
     * default values and settings
     */
    var defaults = {
        id: 0,
        name: '',
        points: null
    }
    var settings = $.extend( {}, defaults, options );
    /*
     *
     */
    var _id = settings.id;
    /*
     *
     */
    var _name = settings.name;
    /*
     * team color used in the chart
     */
    var _color = settings.color;
    /*
     * array of players on the team
     */
    var _players = [];
    /*
     * team label used in the display
     */
    var _label = null;

    return {
        /*
         * no instanceof when using the module pattern
         */
        toString: function() {
            return 'team';
        },
        get id() {
            return _id;
        },
        set id(value) {
            _id = value;
        },
        get color() {
            return _color;
        },
        set color(value) {
            _color = value;
        },
        get name() {

            if ( ! _name )
                return _id;

            return _name;
        },
        set name(value) {

            if ( typeof value != 'string' )
                value = value.toString();

            _name = value;
        },
        /*
         * html element wrapped in jQuery
         */
        teamLabel: function(key) {

            var cssClasses = 'team-label';
            //home-label || away-label
            cssClasses += key ? ' ' + key + '-label' : '';

            if ( _label && _label instanceof jQuery ) {
                _label
                    .removeClass()
                    .addClass(cssClasses);
                return _label;
            }

            _label = $('<div class="' + cssClasses + '">' +
                            '<span class="team-color color-' + key + '" style="background: ' + this.color + ' " />' +
                            '<span class="team-name">' + this.name + '</span>' +
                            '<div class="cw-box" style="display: none;"><div class="colorwheel" /></div>' +
                        '</div>');

            /*
             * attach click handler to change color
             */
            var icon = _label.find('.team-color');
            var $cw = icon.siblings('.cw-box');
            var cw = Raphael.colorwheel( $cw.children('.colorwheel')[0], 150 );
            //starting color
            cw.color( this.color );
            /*
             * show hide the colorwheel
             */
            icon.on( 'click', function() {
                $cw.fadeToggle()
                    .parent().siblings().children('.cw-box')
                        .hide();
            });
            /*
             * apply the color
             */
            cw.onchange( function(color) {
                var os = $('.color-' + key);
                os.css({
                    background: color.hex
                });
                /*
                 * set permanent color for the team
                 * for future game changes
                 */
                var cache = {};
                cache[ Game[key].id ] = color.hex;
                Cache.teamColors = cache;
                /*
                 * also update team
                 */
                Game[key].color = color.hex;
            });

            return _label;
        }

    }
}
