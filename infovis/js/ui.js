var UI = ( function() {
    /*
     * ui settings
     */
    var _settings = {
        changeSpeed: 500
    };
    /*
     * select dropdown that loads the games
     */
    var _loader = null;
    /*
     * checkboxes that say which stat type to show
     */
    var _statTypes = {};

    var setStatTypes = function(value) {

        _statTypes = {};

        value.each( function(index, el) {
            var item = {
                type: el.value,
                grid: el.dataset.grid
            };
            if ( el.checked )
                item.visible = true;
            _statTypes[el.value] = item;
        });
    }
    /*
     * show individual player stats
     */
    var _player = null;
    /*
     * url to fetch the data
     */
    var _dataUrl = null;

    return {
        /*
         * get/set
         */
        get statTypes() {

            return _statTypes;
        },
        set statTypes(value) {

            if ( typeof value === 'string'
                || value instanceof HTMLElement )
                value = $(value);

            if ( ! (value instanceof jQuery) )
                throw new Error();

            setStatTypes(value);
            /*
             * allow stattype change
             */
            value.on( 'change', function() {

                value.each( function(index, el) {

                    var added = el.checked ? true : false;

                    for ( var key in _statTypes ) {
                        var statType = _statTypes[key];
                        if ( ! statType.visible )
                            continue;
                        var type = statType.type;
                        /*
                         * if already in
                         */
                        if ( type === el.value && el.checked  ) {
                            added = false;
                            break;
                        }
                        /*
                         * if removed
                         */
                        else if ( type === el.value && ! el.checked ) {
                            Draw.hideLayer(el.value);
                            break;
                        }
                    }
                    if ( added ) {
                        Draw.showLayer(el.value);
                    }
                });

                setStatTypes(value);
            });
        },
        get loader() {
            return _loader;
        },
        set loader(value) {

            if ( value instanceof HTMLElement
                    || typeof value == 'string' )
                value = $(value);

            if ( ! (value instanceof jQuery)
                    || ! value.length )
                throw new Error('Passing the <select> that loads the game as the wrong type');
            /*
             * remove any old listeners
             */
            if ( _loader )
                _loader.off();
            _loader = value;
            this.dataUrl = _loader.val();
            /*
             * attach event handler to reload on select change
             */
            var self = this;
            _loader.on( 'change', function() {
                self.dataUrl = _loader.val();
                self.showGraph();
            });
        },
        get player() {
            return _player;
        },
        set player(value) {

            if ( value instanceof HTMLElement
                    || typeof value == 'string' )
                value = $(value);

            if ( ! (value instanceof jQuery)
                    || ! value.length )
                throw new Error('Passing the <select> that sets individual player to show as the wrong type');
            /*
             * remove any old listeners
             */
            if ( _player )
                _player.off();
            _player = value;
            Draw.showPlayer();
            /*
             * attach event handler to reload on select change
             */
            _player.on( 'change', function() {
                Draw.canvas.trigger( Draw.events.POINTS_INSERTED );
            });
        },
        get dataUrl() {
            return _dataUrl;
        },
        set dataUrl(value) {

            if ( typeof value != 'string' )
                throw new Error('dataUrl has to be a string');

            _dataUrl = 'data/' + value;
        },
        /*
         * fetch data and display graph
         */
        showGraph: function() {

            Draw.reset();

            var start = 0;

            $.ajax({
                url: _dataUrl,
                dataType: 'json',
                beforeSend: function() {
                    start = new Date();
                },
                success: function(data) {
                    /*
                     * slow down the init of the next game
                     * so it doesn't init until the old one is finished removing
                     * slideUp + remove
                     */
                    var duration = new Date() - start;
                    var timeout = _settings.changeSpeed > duration ? _settings.changeSpeed - duration : 0;

                    if ( ! data
                            || (data.error && data.error.isError == 'true') ) {
                        var m = ( data && data.error && data.error.message ) ? data.error.message : 'Could not fetch data';
                        alert(m);
                        return;
                    }
                    /*
                     * set the data
                     */
                    Parser[this.dataType] = data;


                    setTimeout( function() {
                        /*
                         * init game
                         */
                        Game.init();
                        /*
                         * draw team names/colors etc.
                         */
                        Draw.legend();
                        /*
                         * draw the graph
                         */
                        Draw.graphPoints();
                        /*
                         * update everything
                         */
                        Updater.all();

                    }, timeout );
                }
            });
        },
        /*
         * ui settings
         */
        get settings() {
            return _settings;
        }
    }
})();
