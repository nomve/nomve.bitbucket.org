var Updater = ( function() {

    return {
        all: function() {
            this.names();
        },
        names: function() {

            var home = $.Deferred();
            var away = $.Deferred();
            /*
             * get team names
             */
            var homeId = Game.home.id;
            /*
             * if we already have it
             */
            if ( Cache.teamNames.hasOwnProperty(homeId) ) {
                Game.home.name = Cache.teamNames[homeId].name;
                home.resolve(Game.home.name);
            }
            else {
                //ajax call
                //todo
            }
            /*
             * the same
             */
            var awayId = Game.away.id;
            if ( Cache.teamNames.hasOwnProperty(awayId) ) {
                Game.away.name = Cache.teamNames[awayId].name;
                away.resolve(Game.away.name);
            }
            else {
                //todo
            }

            /*
             * update the screen
             */
            $.when( home, away ).done( function( homeName, awayName ) {
                $('.home-label').find('.team-name').text(homeName);
                $('.away-label').find('.team-name').text(awayName);
            });
        }
    }

})();
