function Player(options) {
    /*
     * default values and settings
     */
    var defaults = {
        id: 0,
        name: ''
    }
    var settings = $.extend( {}, defaults, options );
    /*
     * player id
     */
    var _id = settings.id;
    /*
     * team id, player belongs to this team
     */
    var _teamId = settings.teamId;
    /*
     * player name
     */
    var _name = settings.name;

    return {
        toString: function() {
            return 'player';
        },
        get id() {
            return _id;
        },
        get name() {

            if ( ! _name )
                return _id;
            return _name;
        },
        get teamId() {
            return _teamId;
        }
    }
}
