var Game = ( function() {
    /*
     * home and away team
     */
    var _home = null;
    var _away = null;
    /*
     * total score
     */
    var _homeScore = 0;
    var _awayScore = 0;
    /*
     * all the important data
     * points, rebounds, assists, etc.
     */
    var _data = [];
    /*
     * general info
     */
    var _season = '';
    var _arena = '';
    var _attendance = 0;

    return {
        /*
         * set teams, game info etc.
         * everything that won't change
         */
        init: function() {
            /*
             * set home and away teams
             * and their options
             */
            var teams = Parser.teams;
            var homeTeam = teams.home;
            //color or orange
            var homeColor = Cache.teamColor(homeTeam.id);
            homeTeam.color = homeColor ? homeColor : '#ffa500';
            //the same for away team
            var awayTeam = teams.away;
            //color or green
            var awayColor = Cache.teamColor(awayTeam.id);
            awayTeam.color =  awayColor ? awayColor : '#7ebd2b';
            this.home = homeTeam;
            this.away = awayTeam;
            /*
             * get play data
             */
            this.data = Parser.parsedData;
            /*
             * set general info
             */
            var info = Parser.info;
            this.season = info.season;
            this.arena = info.arena;
            this.attendance = info.attendance;
        },
        /*
         * setting data
         */
        set data(value) {

            _data = value;
        },
        get data() {

            return _data;
        },
        /*
         * get/set home/away team
         */
        get home() {
            return _home;
        },
        set home(value) {

            if ( value != 'team' )
                throw new TypeError('Home team of the wrong type');

            _home = value;
        },
        get away() {
            return _away;
        },
        set away(value) {

            if ( value != 'team' )
                throw new TypeError('Away team of the wrong type');

            _away = value;
        },
        get teams() {
            return {
                home: _home,
                away: _away
            }
        },
        /*
         * general info get/set
         */
        get season() {
            return _season;
        },
        set season( value ) {

            if ( typeof value != 'string' )
                console.warn('Trying to set Game.season not as a string');

            _season = value;
        },
        get arena() {
            return _arena;
        },
        set arena(value) {

            if ( typeof value != 'string' )
                console.warn('Trying to set Game.arena not as a string');

            _arena = value;
        },
        get attendance() {
            return _attendance;
        },
        set attendance(value) {

            if ( typeof value != 'number' )
                console.warn('Trying to set Game.attendance not as number');

            _attendance = value;
        },
        /*
         * updatescore
         */
        get score() {

            return {
                home: _homeScore,
                away: _awayScore
            }
        },
        set score(value) {

            if ( ! (value instanceof Object
                    && value.hasOwnProperty('home')
                    && value.hasOwnProperty('away')) )
                throw new TypeError('Trying to set Game.score not as Object: {home: 0, away: 0}');

            this.homeScore = value.home;
            this.awayScore = value.away;
        },
        set homeScore(value) {

            if ( typeof value != 'number' )
                throw new TypeError('Trying to set Game.homeScore not as number');

            _homeScore = value;
        },
        set awayScore(value) {

            if ( typeof value != 'number' )
                throw new TypeError('Trying to set Game.homeScore not as number');

            _awayScore = value;
        }
    }
})()
