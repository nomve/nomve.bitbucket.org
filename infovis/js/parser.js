var Parser = (function() {
    /*
     * stores the json data
     */
    var data = null;
    /*
     * get team id
     */
    var getTeamId = function(key) {
        /*
         * don't do anything without team id
         */
        if ( ! ( data
               && data.payload
               && data.payload.gameProfile
               && data.payload.gameProfile[key]) )
           throw new Error('Missing team ID: ' + key);

        return parseInt( data.payload.gameProfile[key], 10 );
    };

    return {
        /*
         * set current data
         */
        set json(d) {

            if ( ! d )
                throw new TypeError('Missing data to parse or set');

            if ( typeof d == 'string' )
                data = JSON.parse(d);

            data = d;
        },
        /*
         * get basic info
         */
        get info() {

            if ( ! data )
                throw new Error('Missing data to parse');

            var info = {};

            if ( data.payload ) {
                if ( data.payload.gameProfile && data.payload.gameProfile.arenaName ) {
                    info.arena = data.payload.gameProfile.arenaName;
                }
                if ( data.payload.boxscore && data.payload.boxscore.attendance ) {
                    info.attendance = parseInt( data.payload.boxscore.attendance.replace(',',''), 10 );
                }
                if ( data.payload.season && data.payload.season.scheduleYearDisplay ) {
                    info.season = data.payload.season.scheduleYearDisplay;
                }
            }

            return info;
        },
        /*
         * get teams
         */
        teams: {
            get home() {

                var options = {};
                options.id = getTeamId('homeTeamId');

                return Team(options);
            },
            get away() {

                var options = {};
                options.id = getTeamId('awayTeamId');

                return Team(options);

            }
        },
        /*
         * get play by play data
         */
        get parsedData() {
            /*
             * is there data
             */
            if ( ! ( data
                   && data.payload
                   && data.payload.playByPlays) )
               throw new Error('Missing data to parse');
            /*
             * play by plays contains 4 quarter objects
             */
            var quarters = data.payload.playByPlays;
            /*
             * every array contains quarter objects
             * every quarter contains minute objects
             * minute object {points: { plays: [], count:{id: int, id: int} } }
             */
            var parsedData = [];
            /*
             * object that holds one minute of play
             */
            var homeTeamId = getTeamId('homeTeamId');
            var awayTeamId = getTeamId('awayTeamId');
            var minute = {
                minute: 1,
                points: {
                    plays: [],
                    count: {}
                },
                assists: {
                    plays: [],
                    count: {}
                },
                rebounds: {
                    plays: [],
                    count: {}
                }
            };
            minute.points.count[homeTeamId] = 0;
            minute.points.count[awayTeamId] = 0;
            minute.assists.count[homeTeamId] = 0;
            minute.assists.count[awayTeamId] = 0;
            minute.rebounds.count[homeTeamId] = 0;
            minute.rebounds.count[awayTeamId] = 0;
            /*
             * we want to reverse the minutes
             * so 11:44 to play is the first minute
             */
            var currentMinute = 11;
            var substractMinute = 10;
            /*
             * go backwards because all plays are backwards
             */
            for ( var i = quarters.length; i--; ) {
                /*
                 * current quarter
                 */
                var period = quarters[i].period;
                /*
                 * quarter array to store minutes
                 */
                var quarterPoints = {
                    period: period,
                    minutes: []
                };
                /*
                 * go through the plays
                 */
                var events = quarters[i].events;
                for ( var j = events.length; j--; ) {

                    var event = events[j];
                    /*
                     * only interested in stats
                     */
                    if ( ! event.statCategory )
                        continue;

                    /*
                     * check if we are in the same minute
                     */
                    var eventMinute = parseInt( event.gameClock, 10 );
                    if ( currentMinute !== eventMinute ) {
                        /*
                         * copy current minute object to minutes array
                         * if currentminute === 0 -> start of the quarter
                         * reset plays but don't push because it will push the last minute of the previous quarter again
                         */
                        if ( currentMinute )
                            quarterPoints.minutes.push( $.extend( true, {}, minute) );
                        /*
                         * reset plays
                         */
                        minute.points.plays = [];
                        minute.rebounds.plays = [];
                        minute.assists.plays = [];
                        /*
                         * need to recalculate the way minutes are transformed
                         * 11->1 (11-10=1)
                         * 10->2 (10-8=2)
                         */
                        substractMinute -= (currentMinute - eventMinute) * 2;
                        currentMinute = eventMinute;
                    }
                    /*
                     * update for the new minute obj
                     */
                    minute.minute = currentMinute - substractMinute;

                    if ( event.statCategory === 'PTS' ) {
                        var pointNum = parseInt( event.points, 10 );
                        /*
                         * push the play in
                         */
                        minute.points.plays.push({
                            playerId: parseInt(event.playerId, 10),
                            teamId: parseInt(event.offensiveTeamId, 10),
                            points: pointNum
                        });
                        //if ( event.playerId === "201168") {
                        //    console.log( (period-1) * 12 + minute.minute, pointNum );
                        //    console.log(' ');
                        //}
                        /*
                         * update count
                         */
                        minute.points.count[event.offensiveTeamId] += pointNum;

                        if ( event.statCategory2 === 'AST' ) {
                            /*
                             * push the play in
                             */
                            minute.assists.plays.push({
                                playerId: parseInt(event.playerId2, 10),
                                teamId: parseInt(event.offensiveTeamId, 10)
                            });
                            /*
                             * update count
                             */
                            minute.assists.count[event.offensiveTeamId] += 1;
                        }
                    }
                    else if ( event.statCategory === 'REB' ) {
                        /*
                         * push the play in
                         */
                        minute.rebounds.plays.push({
                            playerId: parseInt(event.playerId, 10),
                            teamId: parseInt(event.teamId, 10)
                        });
                        /*
                         * update count
                         */
                        minute.rebounds.count[event.teamId] += 1;
                    }
                }
                /*
                 * also push the last minute
                 */
                quarterPoints.minutes.push( $.extend(true, {}, minute) );
                /*
                 * store quarters in the array
                 */
                parsedData.push( quarterPoints );

            }

            return parsedData;
        }

    }
})();
